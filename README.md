## Multiplicar console app

Esta es una aplicación para generar archivos de tablas de multiplicar

Ejecutar ``` npm install ```  para las librerias

#### Ejemplo de crear tabla 
```
node app  crear -b 5 -l 15 
```

#### Ejemplo de listar tabla
```
node app  listar -b 5 -l 15
```