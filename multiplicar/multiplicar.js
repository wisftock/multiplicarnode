const fs = require('fs');
const colors = require('colors');

const crearArchivo = (base, limite = 10) => {
  return new Promise((resolve, reject) => {
    if (!Number(base)) {
      reject('El dato ingresado no es un número');
      return;
    } else {
      let data = '';
      for (let i = 1; i <= limite; i++) {
        data += `${base} * ${i} = ${base * i}\n`;
      }
      fs.writeFile(`tabla/tabla-${base}.txt`, data, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(`tabla-${base}.txt`);
        }
      });
    }
  });
};

const listarArchivo = (base, limite = 10) => {
  console.log('======================='.green);
  console.log(`===== Tabla de ${base} ======`.gray);
  console.log('======================='.green);

  return new Promise((resolve, reject) => {
    for (let i = 1; i <= limite; i++) {
      console.log(`${base} * ${i} = ${base * i}\n`);
    }
  });
};

module.exports = {
  crearArchivo,
  listarArchivo,
};
