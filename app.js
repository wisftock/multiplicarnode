const argv = require('./config/yargs').argv;
const colors = require('colors');
const { crearArchivo, listarArchivo } = require('./multiplicar/multiplicar');

let comando = argv._[0];
switch (comando) {
  case 'listar':
    listarArchivo(argv.base, argv.limite);
    break;
  case 'crear':
    crearArchivo(argv.base, argv.limite)
      .then((archivo) =>
        console.log(`El archivo de ${archivo.green} ha sido creada`)
      )
      .catch((err) => console.log(err));
    break;
  default:
    console.log('comando no reconocido');
}

// let base = 7;

// console.log(process.argv);
// let argv = process.argv;
// let parametro = argv[2];
// let base = parametro.split('=')[1];

// crearArchivo(base)
//   .then(() => console.log(`El archivo de la tabla de ${base} ha sido creada`))
//   .catch((err) => console.log(err));
